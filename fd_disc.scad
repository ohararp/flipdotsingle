$fa = 2.5;
$fs = 0.1;

disc_dia = 50;				// Flip-disc diameter
disc_r = disc_dia / 2;
disc_h = 0.8;				// Disc thickness
rod_w = 1.5;				// Disc-rod thickness
rod_space = 1.0;			// Disc rod spacing before base
hole_r = rod_w/2 + 0.15;
magnet_dia = 3;			// Magnet diameter
magnet_r = magnet_dia/2;
magnet_h = 1;				// Magnet height
magnet_space = -0.5;		// Disc/magnet spacing
base_w = 12;				// Base width
base_h = 3.0;				// Base thickness
base_top = 2;				// Base spacing abobe disc mounting
base_space = 2;			// Disc spacing above base
mh_r = 3.1 / 2;			// Mounting holes
mh_ofs = disc_r * 0.7;		// Mounting holes offset from disc center
ch_r = 2.1 / 2;			// Coil hole
hold_w = base_w / 3;		// Turn stop holder from disc-center

mag_ring = rod_w * sqrt(2);
rod_l = disc_dia + 2*rod_space + 2*base_h/2 + magnet_dia + magnet_space + mag_ring;
rod_move = -disc_dia/2 - rod_space - base_h/2;
mag_o_r = (magnet_dia + mag_ring)/2;
hole_ofs = mag_o_r + disc_r + magnet_space;

module rotcut(_r, _a) {
	pt = [	[0, 0, 3*rod_w/2],
			[5*_r*sin(_a), 5*_r*cos(_a), 3*rod_w/2],
			[0, 5*_r, 3*rod_w/2],
			[5*_r*sin(-_a), 5*_r*cos(-_a), 3*rod_w/2],
			[0, 0, -3*rod_w/2],
			[5*_r*sin(_a), 5*_r*cos(_a), -3*rod_w/2],
			[0, 5*_r, -3*rod_w/2],
			[5*_r*sin(-_a), 5*_r*cos(-_a), -3*rod_w/2]
		];
	fc = [	[0, 1, 5, 4],
			[1, 2, 6, 5],
			[2, 3, 7, 6],
			[3, 0, 4, 7],
			[2, 1, 0, 3],
			[4, 5, 6, 7]
		];
	translate([rod_w, 0, 0]) rotate(a = [90, 0, 90]) difference() {
		cylinder(r =_r+rod_w/2+0.1, h = rod_w * 2, center = true);
		cylinder(r =_r-rod_w/2-0.1, h = rod_w * 3, center = true);
		polyhedron(points = pt, faces = fc);
	}
}

module disc() {
	translate([0, 0, -rod_w/2])
	difference() {
		union() {
			translate([0, 0, disc_h/2])
				cylinder(r = disc_r, h = disc_h, center = true);
			translate([rod_move, -rod_w/2, 0])
				cube([rod_l, rod_w, rod_w], center = false);
			translate([hole_ofs, 0, 0])
				cylinder(r = mag_o_r, h = rod_w, center = false);
			translate([-disc_r, -hold_w-rod_w/2, 0])
				cube([rod_w, hold_w+rod_w, rod_w], center = false);
			translate([-(disc_r+rod_space+base_h/2), -hold_w-rod_w/2, 0])
				cube([rod_space+base_h/2+rod_w, rod_w, rod_w], center = false);
		}
		/* Magnet cutout */
		if(magnet_h < rod_w) {
			translate([hole_ofs, 0, rod_w/2+(rod_w-magnet_h)/2])
				cylinder(r = magnet_r, h = rod_w, center = true);
		} else {
			translate([hole_ofs, 0, (-rod_w+magnet_h)/2])
				cylinder(r = magnet_r, h = magnet_h, center = true);
		}
	}
}

module base() {
	hh = disc_r + base_h + base_space + base_top;
	translate([0, 0, -hh+base_space]) {
		union() {
			difference() {
				translate([rod_move-base_h/2, -base_w/2, 0])
					cube([rod_l + base_h, base_w, base_h], center = false);
				/* Base mount holes */
				#translate([-mh_ofs, 0, base_h/2])
					cylinder(r = mh_r, h = base_h * 2, center = true);
				#translate([mh_ofs, 0, base_h/2])
					cylinder(r = mh_r, h = base_h * 2, center = true);
				/* Base coil mount */
				translate([hole_ofs, 0, base_h/2])
					cylinder(r = ch_r, h = base_h * 2, center = true);
			}
			/* Right holder */
			translate([rod_move-base_h/2+rod_l, -base_w/2, 0]) {
				difference() {
					cube([base_h, base_w, hh], center = false);
					#translate([0, base_w/2, hh - base_top])
						rotate(a = [0, 90, 0]) cylinder(r = hole_r, h = base_h, center = true);
				}
			}
			/* Left holder */
			translate([rod_move-base_h/2, -base_w/2, 0]) {
				difference() {
					cube([base_h, base_w, hh], center = false);
					translate([base_h/2, base_w/2, hh-base_top])
						rotcut(hold_w, 82);
					#translate([base_h, base_w/2, hh - base_top])
						rotate(a = [0, 90, 0]) cylinder(r = hole_r, h = base_h, center = true);
				}
			}
		}
	}

}

module coil() {
	hh = disc_r+base_h-magnet_r-mag_o_r;
	union() {
		translate([hole_ofs, 0, -hh/2-magnet_r-mag_o_r+base_space/2]) {
			cylinder(r = ch_r, h = hh, center = true);
			cylinder(r = 5*ch_r/2, h = 7*hh/10, center = true);
			translate([0, 0, 8.4*hh/20])
				cylinder(r1 = 5*ch_r/2, r2 = ch_r, h = 1.4*hh/10, center = true);
			translate([0, 0, -8.4*hh/20])
				cylinder(r2 = 5*ch_r/2, r1 = ch_r, h = 1.4*hh/10, center = true);
		}
	}
}


module complete() {
	rotate(a=[70, 0, 0]) disc();
	base();
	coil();
}

complete();
*translate([0, 0, rod_w/2]) disc();
*translate([0, 0, base_w/2]) rotate(a=[90, 0, 0]) base();