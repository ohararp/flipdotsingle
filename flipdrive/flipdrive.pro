update=Tue 14 Apr 2015 09:27:44 PM CEST
version=1
last_client=eeschema
[cvpcb]
version=1
NetIExt=net
[cvpcb/libraries]
EquName1=devcms
[general]
version=1
[pcbnew]
version=1
PageLayoutDescrFile=
LastNetListRead=flipdrive.net
UseCmpFile=1
PadDrill=3.2
PadDrillOvalY=3.2
PadSizeH=3.7
PadSizeV=3.7
PcbTextSizeV=1.5
PcbTextSizeH=1.5
PcbTextThickness=0.3
ModuleTextSizeV=1
ModuleTextSizeH=1
ModuleTextSizeThickness=0.15
SolderMaskClearance=0.127
SolderMaskMinWidth=0.1016
DrawSegmentWidth=0.2
BoardOutlineThickness=0.09999999999999999
ModuleOutlineThickness=0.2
[pcbnew/libraries]
LibDir=
[eeschema]
version=1
PageLayoutDescrFile=
SubpartIdSeparator=0
SubpartFirstId=65
LibDir=
NetFmtName=
RptD_X=0
RptD_Y=100
RptLab=1
LabSize=60
[eeschema/libraries]
LibName1=flipdrive-cache
LibName2=lib/nmosfet
LibName3=lib/vagrearg_logo
LibName4=lib/double_diode_ca
LibName5=lib/double_diode_cc
LibName6=lib/dmg6602
LibName7=lib/flipdot_sr
LibName8=lib/ncp1406
LibName9=lib/pic16f1508
LibName10=power
LibName11=device
LibName12=transistors
LibName13=conn
LibName14=linear
LibName15=regul
LibName16=74xx
LibName17=cmos4000
LibName18=adc-dac
LibName19=memory
LibName20=xilinx
LibName21=special
LibName22=microcontrollers
LibName23=dsp
LibName24=microchip
LibName25=analog_switches
LibName26=motorola
LibName27=texas
LibName28=intel
LibName29=audio
LibName30=interface
LibName31=digital-audio
LibName32=philips
LibName33=display
LibName34=cypress
LibName35=siliconi
LibName36=opto
LibName37=atmel
LibName38=contrib
LibName39=valves
